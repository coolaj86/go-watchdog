// LIBARRY NOT DOCUMENTED.
// LIBRARY NOT VERSIONED.
// DO NOT USE YET.
// The watchdog package is meant to be used as a binary only.
// The git tag version describes the state of the binary,
// not the state of the library. The API is not yet stable.
//
// See https://git.rootproject.org/root/go-watchdog for pre-built binaries.
package watchdog
