package main

// Fallback to recent version if not in a git repository
func init() {
	GitRev = "d5c026948cf134997c7260e78d4bd5864ac5b9b3"
	GitVersion = "v1.1.3"
	GitTimestamp = "2019-06-21T01:03:19-06:00"
}
